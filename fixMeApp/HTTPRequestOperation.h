//
//  HTTPRequestOperation.h
//  fixMeApp
//
//  Created by Алексей Молокович on 04.09.2018.
//  Copyright © 2018 alilyin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ComplitionBlock)(NSError *error, NSData *responseData);

@interface HTTPRequestOperation : NSOperation

@property (nonatomic, assign) BOOL isExecuting;
@property (nonatomic, assign) BOOL isFinished;

-(instancetype)initWithUrl:(NSURL*)url withCompletion:(ComplitionBlock) complition;
-(void)cancel;
@end
