//
//  HTTPRequestOperation.m
//  fixMeApp
//
//  Created by Алексей Молокович on 04.09.2018.
//  Copyright © 2018 alilyin. All rights reserved.
//

#import "HTTPRequestOperation.h"

#define CONNECTION_TIMEOUT 11.0f

typedef enum {
    HTTPMethod_GET,
    HTTPMethod_POST,
    HTTPMethod_DELETE
}HTTPMethod;


@interface HTTPRequestOperation()

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, copy) ComplitionBlock block;
@property (nonatomic, strong) NSURLSessionDataTask *task;
@property (nonatomic, getter = isFinished, readwrite)  BOOL finished;
@property (nonatomic, getter = isExecuting, readwrite) BOOL executing;
@end

@implementation HTTPRequestOperation

@synthesize finished  = _finished;
@synthesize executing = _executing;



-(instancetype)initWithUrl:(NSURL*)url withCompletion:(ComplitionBlock) complition{
    if (self = [super init]){
        _finished  = NO;
        _executing = NO;
        _url = url;
        _block = complition;
    }
    return self;
}



-(void)cancel{
    [self.task cancel];

    if (self.executing) {
        self.executing = NO;
        self.finished  = YES;
    }

    
}

-(void)start{
    
    if ([self isCancelled]) {
        self.finished = YES;
        return;
    }
    
    self.executing = YES;
    
    NSURLSession *urlSession = [NSURLSession sharedSession];
    
    urlSession.configuration.timeoutIntervalForRequest = 60;
    urlSession.configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    urlSession.configuration.HTTPShouldUsePipelining = NO;
    urlSession.configuration.HTTPShouldSetCookies = NO;
    urlSession.configuration.HTTPCookieStorage = nil;
    urlSession.configuration.URLCache = nil;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:CONNECTION_TIMEOUT];
   [request setHTTPMethod:@"GET"];
    
    __weak typeof(self) weakSelf = self;
    
    self.task = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          
                                          __strong typeof(weakSelf) blockSelf = weakSelf;
                                          if (!blockSelf) return;
                                          
                                          blockSelf.executing = NO;
                                          blockSelf.finished  = YES;
                                          
                                          if (error != nil) {
                                              if (blockSelf.block) {
                                                  blockSelf.block(error, nil);
                                              }
                                              return;
                                          }
                                          
                                          if (blockSelf.block) {
                                              blockSelf.block(nil, data);
                                          }
                                      }];
    [self.task resume];
}

#pragma mark - NSOperation methods


- (BOOL)isExecuting {
    @synchronized(self) {
        return _executing;
    }
}

- (BOOL)isFinished {
    @synchronized(self) {
        return _finished;
    }
}

- (void)setExecuting:(BOOL)executing {
    if (_executing != executing) {
        [self willChangeValueForKey:@"isExecuting"];
        @synchronized(self) {
            _executing = executing;
        }
        [self didChangeValueForKey:@"isExecuting"];
    }
}

- (void)setFinished:(BOOL)finished {
    if (_finished != finished) {
        [self willChangeValueForKey:@"isFinished"];
        @synchronized(self) {
            _finished = finished;
        }
        [self didChangeValueForKey:@"isFinished"];
    }
}
@end
