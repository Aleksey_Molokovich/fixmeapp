//
//  MainTableViewCell.m
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import "MainTableViewCell.h"
#import "WebAPI.h"
#import "HTTPRequestOperation.h"

@interface MainTableViewCell()
@property (nonatomic, weak) IBOutlet UIImageView *aircraftStateImageView;
@property (nonatomic, weak) IBOutlet UIImageView *originCountryImageView;
@property (nonatomic, weak) IBOutlet UILabel *callSignLabel;
@property (nonatomic, strong) HTTPRequestOperation *requestOperation;
@property (nonatomic, assign) BOOL isCanceled;
   
@end

@implementation MainTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setState:(OpenSkyState *)state
{
    self.isCanceled = NO;
    [self.aircraftStateImageView setImage:[UIImage imageNamed:@"unknown_state"]];
    [self.originCountryImageView setImage:[UIImage imageNamed:@"unknown_origin_country"]];
    [self.callSignLabel setText:NSLocalizedString(@"Unknown", nil)];
    _state = state;
    [self refresh];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)breakRequest
{
    self.isCanceled = YES;
    if (self.requestOperation) {
        [self.requestOperation cancel];
    }
}

- (void)refresh
{
    __weak typeof(self) weakSelf = self;

     self.requestOperation = [[WebAPI sharedInstance] getCountryInfoByName:self.state.originCoutry withCompletion:^(NSError *error, NSArray *dataArray) {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         if (!blockSelf) return;
         
         if (blockSelf.isCanceled) return;
         
         if (error == nil) {
             for (NSDictionary *country in dataArray) {
                 if ([[country objectForKey:@"name"] isEqualToString:blockSelf.state.originCoutry] == YES ||
                     [[country objectForKey:@"nativeName"] isEqualToString:blockSelf.state.originCoutry] == YES) {
                     [blockSelf.state setCountryAlpa2Code:[country objectForKey:@"alpha2Code"]];
                     break;
                 }
             }
         }
         if ([[NSThread currentThread] isMainThread]){
             [blockSelf insertImages];
         }else{
             dispatch_async(dispatch_get_main_queue(), ^{
                 [blockSelf insertImages];
             });
         }
    }];
    
}
    
-(void)insertImages
{
    [self.callSignLabel setText:self.state.callSign];
    [self.aircraftStateImageView setImage:self.state.onGround ? [UIImage imageNamed:@"on_ground"] : [UIImage imageNamed:@"in_sky"]];
    [self.originCountryImageView setImage:[UIImage imageNamed:([self.state.originCoutryAlpha2Code length] > 0)?self.state.originCoutryAlpha2Code:@"unknown_origin_country"]];
}

@end
