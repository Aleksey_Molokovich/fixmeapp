//
//  MainTableViewCell.h
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSkyState.h"

@interface MainTableViewCell : UITableViewCell
@property (nonatomic, strong) OpenSkyState *state;
-(void)breakRequest;
@end
