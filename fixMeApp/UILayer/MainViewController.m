//
//  MainViewController.m
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import "MainViewController.h"
#import "DetailedStateViewController.h"
#import "MainTableViewCell.h"
#import "OpenSkyState.h"
#import "WebAPI.h"
#import "global.h"

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *statesArray;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIAlertController *spinnerAlert;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"OpenSkyNetworkTitle", nil);
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"stateCell"];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    [self refresh:nil];
}

- (void)showSpinnerAlert
{
    if (self.spinnerAlert == nil)
    {
        self.spinnerAlert = [UIAlertController alertControllerWithTitle:nil
                                                                message:NSLocalizedString(@"LoadingDataPleaseWait", nil)
                                                         preferredStyle:UIAlertControllerStyleAlert];
    }
    __weak typeof(self) weakSelf = self;

    [self presentViewController:self.spinnerAlert animated:NO completion:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        [blockSelf.refreshControl endRefreshing];
    }];
}

- (void)hideSpinnerAlert
{
    __weak typeof(self) weakSelf = self;
    [self.spinnerAlert dismissViewControllerAnimated:YES completion:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        blockSelf.spinnerAlert = nil; 
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.statesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"stateCell"];
    OpenSkyState *state = [[OpenSkyState alloc] initWithDataArray:[self.statesArray objectAtIndex:indexPath.row]];
    cell.state = state;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    MainTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    DetailedStateViewController *detaledStateViewController = [[DetailedStateViewController alloc] initWithOpenSkyState:[cell.state copy]];
    [self.navigationController presentViewController:detaledStateViewController animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(MainTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell breakRequest];
}

- (void)refresh:(id)sender
{
    __weak typeof(self) weakSelf = self;
    
    [self showSpinnerAlert];
    [[WebAPI sharedInstance] getOpenSkyDataWithCompletion:^(NSError *error, NSDictionary *jsonData) {
        
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            blockSelf.statesArray = [jsonData objectForKey:@kStatesKey];
            [blockSelf hideSpinnerAlert];
            
            if (error != nil) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error",nil)
                                                                                         message:[error localizedDescription]
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelButton = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(@"Ok", nil)
                                               style:UIAlertActionStyleCancel
                                               handler:nil];
                [alertController addAction:cancelButton];
                [blockSelf presentViewController:alertController animated:YES completion:nil];
                return;
            }
            [blockSelf.tableView reloadData];
        });
    }];
}

@end
