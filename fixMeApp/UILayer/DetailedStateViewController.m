//
//  DetailedStateViewController.m
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import "DetailedStateViewController.h"
#import "WebAPI.h"
#import "global.h"
#import "MapKit/MapKit.h"

@interface AircraftAnnotation:NSObject <MKAnnotation>
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy, nullable) NSString *title;
@property (nonatomic, readonly, copy, nullable) NSString *subtitle;
@end

@implementation AircraftAnnotation
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    _coordinate = newCoordinate;
}

@end

@interface DetailedStateViewController () <MKMapViewDelegate>
@property (nonatomic, strong) OpenSkyState *state;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) id <MKAnnotation> aircraftAnnotation;
@property (nonatomic, strong) NSTimer *refreshTimer;
- (IBAction)onCloseButton:(id)sender;
@end

@implementation DetailedStateViewController

- (id)initWithOpenSkyState:(OpenSkyState*)state {
    if (self = [super init]) {
        self.state = state;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refresh];
    MKCoordinateRegion region;
    region.center.latitude = self.state.latitude;
    region.center.longitude = self.state.longitide;
    region.span.latitudeDelta = 0.25f;
    region.span.longitudeDelta = 0.25f;
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:TRUE];
    [self.closeButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    __weak typeof(self) weakSelf = self;

    self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f repeats:YES block:^(NSTimer * _Nonnull timer) {
        
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return;
        
        [[WebAPI sharedInstance] getOpenSkyStateForIcao24:blockSelf.state.icao24 withCompletion:^(NSError *error, NSDictionary *jsonData) {
            if ([[jsonData objectForKey:@kStatesKey] count] > 0) {
                NSArray *state = [[jsonData objectForKey:@kStatesKey] objectAtIndex:0];
                blockSelf.state = [[OpenSkyState alloc] initWithDataArray:state];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [blockSelf refresh];
                });
            }
        }];
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self applyMapViewMemoryFix];
        
}
    
- (void)applyMapViewMemoryFix
{
    
    switch (self.mapView.mapType)
    {
        case MKMapTypeHybrid:
            self.mapView.mapType = MKMapTypeStandard;
            break;
        
        case MKMapTypeStandard:
            self.mapView.mapType = MKMapTypeHybrid;
            break;
        
        default:
            break;
    }
    self.mapView.showsUserLocation = NO;
    self.mapView.delegate = nil;
    [self.mapView removeFromSuperview];
    self.mapView = nil;
    self.aircraftAnnotation = nil;
    self.state = nil;
}
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self.aircraftAnnotation reuseIdentifier:@"aircraft"];
    UIImageView *aircraftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map_annotation"]];
    aircraftImageView.transform = CGAffineTransformMakeRotation(self.state.heading * M_PI / 180);
    [annotationView addSubview:aircraftImageView];
    return annotationView;
}

- (void)refresh {
    if (self.aircraftAnnotation == nil) {
        self.aircraftAnnotation = [[AircraftAnnotation alloc] init];
    } else {
        [self.mapView removeAnnotation:self.aircraftAnnotation];
    }
    [self.aircraftAnnotation setCoordinate:CLLocationCoordinate2DMake(self.state.latitude, self.state.longitide)];
    [self.mapView addAnnotation:self.aircraftAnnotation];
    [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(self.state.latitude, self.state.longitide) animated:YES];
}

- (IBAction)onCloseButton:(id)sender {
    [self.refreshTimer invalidate];
    self.refreshTimer  = nil;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
