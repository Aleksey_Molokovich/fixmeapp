//
//  OpenSkyState.h
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSkyState : NSObject<NSCopying>
@property (readonly) NSString *icao24;
@property (readonly) NSString *callSign;
@property (readonly) NSString *originCoutry;
@property (readonly) NSString *originCoutryAlpha2Code;
@property (readonly) NSTimeInterval positionUpdateTimestamp;
@property (readonly) NSTimeInterval velocityUpdateTimestamp;
@property (readonly) double longitide;
@property (readonly) double latitude;
@property (readonly) double altitude;
@property (readonly) BOOL onGround;
@property (readonly) double velocity;
@property (readonly) double heading;
@property (readonly) double verticalRate;
- (OpenSkyState*)initWithDataArray:(NSArray*)array;
- (OpenSkyState*)initWithDataArrayICAO24:(NSArray*)array;
- (void)setCountryAlpa2Code:(NSString*)alpha2Code;
@end
