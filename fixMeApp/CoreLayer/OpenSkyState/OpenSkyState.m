//
//  OpenSkyState.m
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import "OpenSkyState.h"

typedef enum
{
    ICAO24 = 0,
    CALLSIGN,
    ORIGIN_COUNTRY,
    TIME_POSITION,
    TIME_VELOCITY,
    LONGITUDE,
    LATITUDE,
    ALTITUDE,
    ON_GROUND,
    VELOCITY,
    HEADING,
    VERTICAL_RATE,
    SENSORS
}OpenSkyVectorFieldIndex;

@interface OpenSkyState()
@property (nonatomic, strong) NSString *icao24;
@property (nonatomic, strong) NSString *callSign;
@property (nonatomic, strong) NSString *originCoutry;
@property (nonatomic, strong) NSString *originCoutryAlpha2Code;
@property (nonatomic, assign) NSTimeInterval positionUpdateTimestamp;
@property (nonatomic, assign) NSTimeInterval velocityUpdateTimestamp;
@property (nonatomic, assign) double longitide;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double altitude;
@property (nonatomic, assign) BOOL onGround;
@property (nonatomic, assign) double velocity;
@property (nonatomic, assign) double heading;
@property (nonatomic, assign) double verticalRate;
@end

@implementation OpenSkyState

- (OpenSkyState*)initWithDataArrayICAO24:(NSArray*)array{
    if (self = [super init]) {
        assert(array != nil);
        assert([array count] >= 12);
        assert(![[array objectAtIndex:ICAO24] isKindOfClass:[NSNull class]] && [[array objectAtIndex:ICAO24] isKindOfClass:[NSString class]]);
        self.icao24 = [[array objectAtIndex:ICAO24] copy];
    }
    return self;
}
    
    
- (OpenSkyState*)initWithDataArray:(NSArray*)array
{
    if (self = [super init]) {
        assert(array != nil);
        assert([array count] >= 12);
        assert(![[array objectAtIndex:ICAO24] isKindOfClass:[NSNull class]] && [[array objectAtIndex:ICAO24] isKindOfClass:[NSString class]]);
        assert([[array objectAtIndex:CALLSIGN] isKindOfClass:[NSNull class]] || [[array objectAtIndex:CALLSIGN] isKindOfClass:[NSString class]]);
        assert(![[array objectAtIndex:ORIGIN_COUNTRY] isKindOfClass:[NSNull class]] && [[array objectAtIndex:ORIGIN_COUNTRY] isKindOfClass:[NSString class]]);
        assert([[array objectAtIndex:TIME_POSITION] isKindOfClass:[NSNull class]] || [[array objectAtIndex:TIME_POSITION] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:TIME_VELOCITY] isKindOfClass:[NSNull class]] || [[array objectAtIndex:TIME_VELOCITY] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:LONGITUDE] isKindOfClass:[NSNull class]] || [[array objectAtIndex:LONGITUDE] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:LATITUDE] isKindOfClass:[NSNull class]] || [[array objectAtIndex:LATITUDE] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:ALTITUDE] isKindOfClass:[NSNull class]] || [[array objectAtIndex:ALTITUDE] isKindOfClass:[NSNumber class]]);
        assert(![[array objectAtIndex:ON_GROUND] isKindOfClass:[NSNull class]] && [[array objectAtIndex:ON_GROUND] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:VELOCITY] isKindOfClass:[NSNull class]] || [[array objectAtIndex:VELOCITY] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:HEADING] isKindOfClass:[NSNull class]] || [[array objectAtIndex:HEADING] isKindOfClass:[NSNumber class]]);
        assert([[array objectAtIndex:VERTICAL_RATE] isKindOfClass:[NSNull class]] || [[array objectAtIndex:VERTICAL_RATE] isKindOfClass:[NSNumber class]]);
        
        self.icao24 = [[array objectAtIndex:ICAO24] copy];
        self.callSign = [array objectAtIndex:CALLSIGN] == nil ? NSLocalizedString(@"Unknown", nil) : [[array objectAtIndex:CALLSIGN] copy];
        if ([[self.callSign stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0) {
            self.callSign = NSLocalizedString(@"Unknown", nil);
        }
        self.originCoutry = [[array objectAtIndex:ORIGIN_COUNTRY] copy];
        self.positionUpdateTimestamp = [[array objectAtIndex:TIME_POSITION] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:TIME_POSITION] doubleValue];
        self.velocityUpdateTimestamp = [[array objectAtIndex:TIME_VELOCITY] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:TIME_VELOCITY] doubleValue];
        self.longitide = [[array objectAtIndex:LONGITUDE] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:LONGITUDE] doubleValue];
        self.latitude = [[array objectAtIndex:LATITUDE] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:LATITUDE] doubleValue];
        self.altitude = [[array objectAtIndex:ALTITUDE] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:ALTITUDE] doubleValue];
        self.onGround = [[array objectAtIndex:ON_GROUND] boolValue];
        self.velocity = [[array objectAtIndex:VELOCITY] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:VELOCITY] doubleValue];
        self.heading = [[array objectAtIndex:HEADING] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:HEADING] doubleValue];
        self.verticalRate = [[array objectAtIndex:VERTICAL_RATE] isKindOfClass:[NSNull class]] ? 0 : [[array objectAtIndex:VERTICAL_RATE] doubleValue];
    }
    return self;
}

- (void)setCountryAlpa2Code:(NSString*)alpha2Code
{
    if (!alpha2Code)
    {
        return;
    }
    
    _originCoutryAlpha2Code = [alpha2Code copy];
}

-(id)copyWithZone:(NSZone *)zone{
    OpenSkyState *new = [OpenSkyState new];
    new->_icao24 = [_icao24 copyWithZone:zone];
    new->_callSign = [_callSign copyWithZone:zone];
    new->_originCoutry = [_originCoutry copyWithZone:zone];
    new->_originCoutryAlpha2Code = [_originCoutryAlpha2Code copyWithZone:zone];
    new->_positionUpdateTimestamp =_positionUpdateTimestamp;
    new->_velocityUpdateTimestamp =_velocityUpdateTimestamp;
    new->_longitide =_longitide;
    new->_latitude =_latitude;
    new->_altitude =_altitude;
    new->_onGround =_onGround;
    new->_velocity =_velocity;
    new->_heading =_heading;
    new->_verticalRate =_verticalRate;

    return new;
}


@end
