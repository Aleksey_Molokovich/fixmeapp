//
//  DBManager.m
//  fixMeApp
//
//  Created by Alexander Ilyin on 30/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import "DBManager.h"
#import "OpenSkyState.h"
#import <sqlite3.h>

@interface DBManager()
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, assign) sqlite3 *db_handle;
@property (nonatomic) dispatch_semaphore_t sqlDBSemaphore;
@end

@implementation DBManager
+ (DBManager *)sharedInstance {
    static DBManager *sharedInstance = nil;
    if (!sharedInstance) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance = [[super allocWithZone:NULL] init];
        });
    }
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        self.sqlDBSemaphore = dispatch_semaphore_create(1);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.databaseFilename = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], @"cashe.sqlite"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:self.databaseFilename] == NO) {
            assert (sqlite3_open([self.databaseFilename UTF8String], &_db_handle) == SQLITE_OK);
            const char *sql = "CREATE TABLE IF NOT EXISTS flights (icao24 TEXT, data TEXT)";
            assert(sqlite3_exec(self.db_handle, sql, NULL, NULL, NULL) == SQLITE_OK);
            const char *sql2 = "CREATE TABLE IF NOT EXISTS countries (name TEXT, info TEXT)";
            assert(sqlite3_exec(self.db_handle, sql2, NULL, NULL, NULL) == SQLITE_OK);
            assert(sqlite3_exec(self.db_handle, "CREATE INDEX index_icao24 ON flights (icao24)", NULL, NULL, NULL) == SQLITE_OK);
            assert(sqlite3_exec(self.db_handle, "CREATE INDEX index_name ON countries (name)", NULL, NULL, NULL) == SQLITE_OK);

            

        } else {;
            assert (sqlite3_open([self.databaseFilename UTF8String], &_db_handle) == SQLITE_OK);
        }
    }
    return self;
}

- (NSString*)getCashedCoutryInfoForName:(NSString*)countryName
{
    dispatch_semaphore_wait(self.sqlDBSemaphore, DISPATCH_TIME_FOREVER);
    int result = SQLITE_OK;
    assert(self.db_handle != NULL);
    char sql[4096] = {0};
    sqlite3_stmt *statement = NULL;
    NSString *coutryNameFixed = [countryName stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    sprintf(sql, "SELECT * FROM countries WHERE name IS \'%s\'", [coutryNameFixed UTF8String]);
    result = sqlite3_prepare(self.db_handle, sql, -1, &statement, NULL);
    assert(result == SQLITE_OK);
    result = sqlite3_step(statement);
    if (result == SQLITE_ROW) {
        char *info = (char*)sqlite3_column_text(statement, 1);
        NSString *str = info != NULL?[NSString stringWithUTF8String:info]:nil;
        sqlite3_finalize(statement);
        dispatch_semaphore_signal(self.sqlDBSemaphore);
        return str;
    }
    sqlite3_finalize(statement);
    dispatch_semaphore_signal(self.sqlDBSemaphore);
    return nil;
}

- (NSString*)getCashedOpenSkyStates
{
    NSString *resultString = nil;
    dispatch_semaphore_wait(self.sqlDBSemaphore, DISPATCH_TIME_FOREVER);
    int result = SQLITE_OK;
    sqlite3_stmt *statement = NULL;
    assert(self.db_handle != NULL);
    result = sqlite3_prepare(self.db_handle, "SELECT * FROM flights", -1, &statement, NULL);
    assert(result == SQLITE_OK);
    int i = 0;
    while (sqlite3_step(statement) == SQLITE_ROW) {
        char *data = (char*)sqlite3_column_text(statement, 1);
        
        if (i == 0) {
            resultString = [NSString stringWithUTF8String:data];
        } else {
            resultString = [resultString stringByAppendingString:@","];
            resultString = [resultString stringByAppendingString:[NSString stringWithUTF8String:data]];
        }
        i++;
    }
    result = sqlite3_finalize(statement);
    assert(result == SQLITE_OK);
    if ([resultString length] > 0) {
        resultString = [NSString stringWithFormat:@"[%@]", resultString];
    }
    dispatch_semaphore_signal(self.sqlDBSemaphore);
    return resultString;
}

- (void)casheOpenSkyStates:(NSArray*)states
{
    dispatch_semaphore_wait(self.sqlDBSemaphore, DISPATCH_TIME_FOREVER);
    sqlite3_stmt *statement = NULL;
    assert(self.db_handle != NULL);
    assert(sqlite3_exec(self.db_handle, "DELETE FROM flights", NULL, NULL, NULL) == SQLITE_OK);
    assert(sqlite3_prepare(self.db_handle, "INSERT INTO flights(icao24, data) values(?, ?)", -1, &statement, NULL) == SQLITE_OK);
    //            assert(sqlite3_exec(self.db_handle, "PRAGMA synchronous = OFF", NULL, NULL, NULL) == SQLITE_OK);
    assert(sqlite3_exec(self.db_handle, "PRAGMA journal_mode = MEMORY", NULL, NULL, NULL) == SQLITE_OK);
    assert(sqlite3_exec(self.db_handle, "BEGIN TRANSACTION", NULL, NULL, NULL) == SQLITE_OK);
    
    for (NSArray *state in states) {
        OpenSkyState *stateObj = [[OpenSkyState alloc] initWithDataArrayICAO24:state];
        NSData *stateData = [NSJSONSerialization dataWithJSONObject:state options:NSJSONWritingPrettyPrinted error:nil];
        NSString *stateStr = [[NSString alloc] initWithData:stateData encoding:NSUTF8StringEncoding];
        assert(sqlite3_bind_text(statement, 1, [stateObj.icao24  UTF8String], -1, NULL) == SQLITE_OK);
        assert(sqlite3_bind_text(statement, 2, [stateStr UTF8String], -1, NULL) == SQLITE_OK);
        assert(sqlite3_step(statement) == SQLITE_DONE);
        sqlite3_clear_bindings(statement);
        assert(sqlite3_reset(statement) == SQLITE_OK);
        stateObj = nil;
    }
    assert(sqlite3_exec(self.db_handle, "END TRANSACTION", NULL, NULL, NULL) == SQLITE_OK);

    assert(sqlite3_reset(statement) == SQLITE_OK);
    assert(sqlite3_finalize(statement) == SQLITE_OK);
    dispatch_semaphore_signal(self.sqlDBSemaphore);
}

- (void)casheStateInfo:(NSArray*)state forIcao24:(NSString*)icao24
{
    dispatch_semaphore_wait(self.sqlDBSemaphore, DISPATCH_TIME_FOREVER);
    sqlite3_stmt *statement = NULL;
    assert(self.db_handle != NULL);
    char sql[4096] = {0};
    sprintf(sql, "DELETE FROM flights WHERE icao24 IS \'%s\'", [icao24 UTF8String]);
    assert(sqlite3_exec(self.db_handle, sql, NULL, NULL, NULL) == SQLITE_OK);
    assert(sqlite3_prepare(self.db_handle, "INSERT INTO flights(icao24, data) values(?, ?)", -1, &statement, NULL)== SQLITE_OK);
    assert(sqlite3_bind_text(statement, 1, [icao24 UTF8String], -1, NULL) == SQLITE_OK);
    NSData *stateData = [NSJSONSerialization dataWithJSONObject:state options:NSJSONWritingPrettyPrinted error:nil];
    NSString *stateStr = [[NSString alloc] initWithData:stateData encoding:NSUTF8StringEncoding];
    assert(sqlite3_bind_text(statement, 2, [stateStr UTF8String], -1, NULL) == SQLITE_OK);
    assert(sqlite3_step(statement) == SQLITE_DONE);
    assert(sqlite3_reset(statement) == SQLITE_OK);
    assert(sqlite3_finalize(statement) == SQLITE_OK);
    dispatch_semaphore_signal(self.sqlDBSemaphore);
}

- (void)casheCoutryInfo:(NSString*)coutryInfo forCountryName:(NSString*)countryName
{
    dispatch_semaphore_wait(self.sqlDBSemaphore, DISPATCH_TIME_FOREVER);
    NSString *coutryNameFixed = [countryName stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    int result = SQLITE_OK;
    sqlite3_stmt *statement = NULL;
    assert(self.db_handle != NULL);
    char sql[4096] = {0};
    sprintf(sql, "DELETE FROM countries WHERE name IS \'%s\'", [coutryNameFixed UTF8String]);
    assert(sqlite3_exec(self.db_handle, sql, NULL, NULL, NULL) == SQLITE_OK);
    result = sqlite3_prepare(self.db_handle, "INSERT INTO countries(name, info) values(?, ?)", -1, &statement, NULL);
    assert(result == SQLITE_OK);
    result = sqlite3_bind_text(statement, 1, [coutryNameFixed UTF8String], -1, NULL);
    assert(result == SQLITE_OK);
    result = sqlite3_bind_text(statement, 2, [coutryInfo UTF8String], -1, NULL);
    assert(result == SQLITE_OK);
    result = sqlite3_step(statement);
    assert(result == SQLITE_DONE);
    result = sqlite3_reset(statement);
    assert(result == SQLITE_OK);
    result = sqlite3_finalize(statement);
    assert(result == SQLITE_OK);
    dispatch_semaphore_signal(self.sqlDBSemaphore);
}

@end
