//
//  DBManager.h
//  fixMeApp
//
//  Created by Alexander Ilyin on 30/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject
+ (DBManager*)sharedInstance;
- (NSString*)getCashedCoutryInfoForName:(NSString*)countryName;
- (void)casheCoutryInfo:(NSString*)coutryInfo forCountryName:(NSString*)countryName;
- (void)casheOpenSkyStates:(NSArray*)states;
- (NSString*)getCashedOpenSkyStates;
- (void)casheStateInfo:(NSArray*)state forIcao24:(NSString*)icao24;
@end

