//
//  WebAPI.h
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HTTPRequestOperation;

@interface WebAPI : NSObject
+ (WebAPI*)sharedInstance;
- (void)getOpenSkyDataWithCompletion:(void (^)(NSError *error, NSDictionary *jsonData))block;
- (void)getOpenSkyStateForIcao24:(NSString*)icao24 withCompletion:(void (^)(NSError *error, NSDictionary *jsonData))block;
- (HTTPRequestOperation*)getCountryInfoByName:(NSString*)countryName withCompletion:(void (^)(NSError *error, NSArray *dataArray))block;
@end
