//
//  WebAPI.m
//  fixMeApp
//
//  Created by Alexander Ilyin on 29/08/2017.
//  Copyright © 2017 alilyin. All rights reserved.
//

#import "WebAPI.h"
#import "Reachability.h"
#import "DBManager.h"
#import "global.h"
#import "OpenSkyState.h"
#import "HTTPRequestOperation.h"

#define CONNECTION_TIMEOUT 11.0f

typedef enum {
    HTTPMethod_GET,
    HTTPMethod_POST,
    HTTPMethod_DELETE
}HTTPMethod;

@interface WebAPI() <NSURLSessionDelegate>
@property (nonatomic, strong) NSOperationQueue *operationQueue;
//@property (nonatomic, strong) NSURLSession *urlSession;
@end

@implementation WebAPI

+ (WebAPI *)sharedInstance {
    static WebAPI *sharedInstance = nil;
    if (!sharedInstance) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
                          sharedInstance = [[super allocWithZone:NULL] init];
                      });
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.operationQueue.maxConcurrentOperationCount = 4;
//        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
//        self.urlSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue:self.operationQueue];
    }
    return self;
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}

- (void)getOpenSkyDataWithCompletion:(void (^)(NSError *error, NSDictionary *jsonData))block
{
        if ([Reachability currentNetworkStatus] == NotReachable) {
            if (block) {
                
                    NSString *cashedOpenSkyDataStr = nil;
                if ((cashedOpenSkyDataStr = [[DBManager sharedInstance] getCashedOpenSkyStates]) != nil) {
                    NSError *dataParsingError = nil;
                    NSArray *parsedData = [NSJSONSerialization JSONObjectWithData:[cashedOpenSkyDataStr dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&dataParsingError];
                    NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:parsedData, @kStatesKey, nil];
                    block(dataParsingError, result);
                    return;
                }
            }
        }
        [self internalWebApiRequestWithURL:[NSURL URLWithString:@"https://opensky-network.org/api/states/all"]
                                withMethod:HTTPMethod_GET
                            withCompletion:^(NSError *error, NSData *responseData) {
                                if (error != nil) {
                                    if (block) {
                                        block(error, nil);
                                    }
                                    return;
                                }
                                
                                NSError *dataParsingError = nil;
                                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&dataParsingError];
                                
                                if (dataParsingError != nil) {
                                    if (block) {
                                        block(dataParsingError, nil);
                                    }
                                    return;
                                }
                                
                                [[DBManager sharedInstance] casheOpenSkyStates:[jsonData objectForKey:@kStatesKey]];

                                if (block) {
                                    block(nil, jsonData);
                                }
                            }];
}

- (void)getOpenSkyStateForIcao24:(NSString*)icao24 withCompletion:(void (^)(NSError *error, NSDictionary *jsonData))block
{
        [self internalWebApiRequestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://opensky-network.org/api/states/all?icao24=%@", icao24]]
                                withMethod:HTTPMethod_GET
                            withCompletion:^(NSError *error, NSData *responseData) {
                                if (error != nil) {
                                    if (block) {
                                        block(error, nil);
                                    }
                                    return;
                                }
                                
                                NSError *dataParsingError = nil;
                                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&dataParsingError];
                                
                                if (dataParsingError != nil) {
                                    if (block) {
                                        block(dataParsingError, nil);
                                    }
                                    return;
                                }
                                
                                
                                NSArray *states = [jsonData objectForKey:@kStatesKey];
                                
                                if (states == nil || [states isKindOfClass:[NSArray class]] == NO){
                                    block(nil,nil);
                                    return;
                                }
                                
                                NSArray *first =[states firstObject];
                                
                                if (first == nil){
                                    block(nil,nil);
                                    return;
                                }
                                [[DBManager sharedInstance] casheStateInfo:first forIcao24:icao24];
                                if (block) {
                                    block(nil, jsonData);
                                }
                            }];
}

- (HTTPRequestOperation *)getCountryInfoByName:(NSString*)countryName withCompletion:(void (^)(NSError *error, NSArray *dataArray))block
{
        if (block) {
            NSString *cashedCountryInfoString = nil;
            if ((cashedCountryInfoString = [[DBManager sharedInstance] getCashedCoutryInfoForName:countryName]) != nil) {
                NSError *dataParsingError = nil;
                id parsedData = [NSJSONSerialization JSONObjectWithData:[cashedCountryInfoString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&dataParsingError];
                block(dataParsingError, parsedData);
                return nil;
            }
        }
    if ([Reachability currentNetworkStatus] == NotReachable) { return nil;}
    
       HTTPRequestOperation *requestOperation = [self internalWebApiRequestWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"https://restcountries.eu/rest/v2/name/%@", countryName] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]
                                withMethod:HTTPMethod_GET
                            withCompletion:^(NSError *error, NSData *responseData) {
                                if (error != nil) {
                                    if (block) {
                                        block(error, nil);
                                    }
                                    return;
                                }
                                NSError *dataParsingError = nil;
                                id parsedData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&dataParsingError];
                                
                                if (dataParsingError != nil) {
                                    if (block) {
                                        block(dataParsingError, nil);
                                    }
                                    return;
                                }
                                
                                if ([parsedData isKindOfClass:[NSArray class]] == YES) {
                                    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];

//                                    NSString *responseString = [NSString stringWithUTF8String:[responseData bytes]];
                                    [[DBManager sharedInstance] casheCoutryInfo:responseString forCountryName:countryName];
                                    if (block) {
                                        block(nil, parsedData);
                                    }
                                } else if ([parsedData isKindOfClass:[NSDictionary class]] == YES) {
                                    if (block) {
                                        NSError *error = [[NSError alloc] initWithDomain:@"fixMeErrorDomain" code:[[parsedData objectForKey:@"status"] intValue] userInfo:parsedData];
                                        block(error, nil);
                                    }
                                } else {
                                    if (block) {
                                        block(nil, nil);
                                    }
                                }
                            }];
    return requestOperation;
}

- (HTTPRequestOperation *)internalWebApiRequestWithURL:(NSURL*)url withMethod:(HTTPMethod)method withCompletion:(void (^)(NSError *error, NSData *responseData))block
{
//    __weak typeof(self) weakSelf = self;
//
//    NSBlockOperation *dataTaskOperation = [[NSBlockOperation alloc] init];
//    [dataTaskOperation addExecutionBlock:^{
//        __strong typeof(weakSelf) blockSelf = weakSelf;
//        if (!blockSelf) return;
//
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData
//                                                           timeoutInterval:CONNECTION_TIMEOUT];
//        switch (method) {
//            case HTTPMethod_GET:
//                [request setHTTPMethod:@"GET"];
//                break;
//            case HTTPMethod_POST:
//                [request setHTTPMethod:@"POST"];
//            case HTTPMethod_DELETE:
//                [request setHTTPMethod:@"DELETE"];
//                break;
//        }
//
//        NSURLSessionDataTask *dataTask = [blockSelf.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                          {
//                                              if (error != nil) {
//                                                  if (block) {
//                                                      block(error, nil);
//                                                  }
//                                                  return;
//                                              }
//
//                                              if (block) {
//                                                  block(nil, data);
//                                              }
//                                          }];
//        [dataTask resume];
//
//    }];
    
    HTTPRequestOperation *dataTaskOperation = [[HTTPRequestOperation alloc] initWithUrl:url withCompletion:block];
    [self.operationQueue addOperation:dataTaskOperation];
    return dataTaskOperation;
}

@end
